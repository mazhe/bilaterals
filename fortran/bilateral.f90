module bilateral_function
implicit none
contains 
  function clamp(x, minval, maxval) result(c)
  implicit none
    integer :: x, minval, maxval, c
    c = min(max(x, minval), maxval)
  end function clamp

  function gauss(x, s) result(g)
  implicit none
    real*8 :: x, s, g
    g = dexp(-(x*x ) / (2*s*s))
  end function gauss

  function gauss2(x, y, s) result(g2)
  implicit none
    real*8 :: x, y, s, g2
    g2 = dexp(-(x*x + y*y) / (2*s*s))
  end function gauss2

  function b(src, xsize, ysize, x, y, diameter, kernel_d, sigma_r) result (bres)
  implicit none
    real*4, intent(in) :: src(:,:)
    integer, intent(in) :: xsize, ysize, x, y, diameter
    real*8, intent(in) :: kernel_d(-diameter/2:diameter/2, -diameter/2:diameter/2)
    real*8, intent(in) :: sigma_r
    real*4 :: bres
    integer :: halfdiameter, i, j, xi, yj
    real*8 :: w
    real*8 :: filtered, wsum
    halfdiameter = diameter / 2
    if (src(y, x) /= src(y, x)) then ! handle NAN but not INFs
      bres = src(y, x)
      return
    endif
    filtered = 0.0
    wsum = 0.0
    do j = -halfdiameter, halfdiameter
      yj = clamp(y+j, 1, ysize)
      do i = -halfdiameter, halfdiameter
        xi = clamp(x+i, 1, xsize)
        if (src(yj, xi) /= src(yj, xi)) then ! handle NAN but not INFs
          continue
        endif
        w = kernel_d(j, i) * gauss(real(src(y, x)-src(yj, xi), 8), sigma_r)
        filtered = filtered + real(src(yj, xi), 8)*w
        wsum = wsum + w
      end do
    end do
    bres = real(filtered / wsum, 4)
  end function

  subroutine bilateral(dst, src, xsize, ysize, sigma_d, sigma_r)
  implicit none
    real*4, intent(out) :: dst(:,:)
    real*4, intent(in) :: src(:,:)
    integer, intent(in) :: xsize, ysize
    real*8, intent(in) :: sigma_d, sigma_r

    integer :: diameter, halfdiameter
    real*8, allocatable :: kernel_d(:,:)
    integer :: x, y

    diameter = int(sigma_d*3.0)
    diameter = diameter + (1-modulo(diameter, 2))
    halfdiameter = diameter/2

    allocate(kernel_d(-halfdiameter:halfdiameter, -halfdiameter:halfdiameter))
    do y = -halfdiameter, halfdiameter
      do x = -halfdiameter, halfdiameter
        kernel_d(y, x) = gauss2(real(x, 8), real(y, 8), sigma_d)
      end do
    end do

    do y = 1, ysize
      do x = 1, xsize
        dst(y, x) = b(src, xsize, ysize, x, y, diameter, kernel_d, sigma_r)
      end do
    end do

    deallocate(kernel_d)
  end subroutine bilateral
end module bilateral_function

program main
use bilateral_function
use sigma
implicit none
  real*4, allocatable :: output(:, :)
  integer :: y, x
  allocate(output(1024, 1024))
  call bilateral(output, input, 1024, 1024, 15.0_8, 70.0_8)
  !do y = 1,1024
  !  write (6, "(1024f11.6)") (output(y, x), x=1,1024)
  !end do
  deallocate(output)
  stop
end program main
