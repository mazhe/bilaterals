package main
import (
	//"fmt"
	"math"
)

func max(x int, y int) int {
	if y < x {
		return x
	}
	return y
}
func min(x int, y int) int {
	if x < y {
		return x
	}
	return y
}
func clamp(x int, minval int, maxval int) int {
	return min(max(x, minval), maxval)
}

func gauss(x float64, s float64) float64 {
	return math.Exp(-(x*x)/(2*s*s))
}
func gauss2(x float64, y float64, s float64) float64 {
	return math.Exp(-(x*x + y*y) / (2*s*s))
}

func b(src []float32, xsize int, ysize int, x int, y int, diameter int, kernel_d []float64, sigma_r float64) float32 {
	src_center := src[xsize*y + x]
    halfdiameter := diameter/2
    filtered := 0.0
    wsum := 1.0

    if math.IsNaN(float64(src_center)) {
        return float32(math.NaN())
    }
	
	for j := -halfdiameter; j <= halfdiameter; j++ {
        y_offset := xsize * clamp(y+j, 0, ysize-1)
		j_offset := (j+halfdiameter) * diameter
		for i := -halfdiameter; i <= halfdiameter; i++ {
			x_offset := clamp(x+i, 0, xsize-1)
			src_neighbour := src[y_offset+x_offset]
			if math.IsNaN(float64(src_neighbour)) {
				continue
			}
			w := kernel_d[j_offset+i+halfdiameter] * gauss(float64(src_neighbour - src_center), sigma_r)
			filtered += float64(src_neighbour) * w
			wsum += w
		}
	}

	return float32(filtered / wsum)
}

func bilateral(dst []float32, src []float32, xsize int, ysize int, sigma_d float64, sigma_r float64) {
	diameter := int(sigma_d*3.0)
	diameter += 1-(diameter%2)
	halfdiameter := int(diameter/2)

	kernel_d := make([]float64, diameter*diameter)
	for y := -halfdiameter; y <= halfdiameter; y++ {
		for x := -halfdiameter; x <= halfdiameter; x++ {
			kernel_d[(y+halfdiameter)*diameter + x+halfdiameter] = gauss2(float64(x), float64(y), sigma_d)
		}
	}

	for y := 0; y < ysize; y++ {
		for x := 0; x < xsize; x++ {
			dst[y*xsize + x] = b(src, xsize, ysize, x, y, diameter, kernel_d, sigma_r)
		}
	}
}

func main() {
	output := make([]float32, 1024*1024)
	bilateral(output, input, 1024, 1024, 15.0, 70.0)

	/*for y := 0; y < 1024; y++ {
		for x := 0; x < 1024; x++ {
			fmt.Printf("%f ", data2[y*1024 + x])
		}
		fmt.Printf("\n")
	}*/
}
