use std::cmp;

#[inline]
fn clamp(x: i32, minval: i32, maxval: i32) -> i32 {
	return cmp::min(cmp::max(x, minval), maxval);
}

#[inline]
fn gauss(x: f64, s: f64) -> f64 {
	return (-(x*x)/(2.0*s*s)).exp();
}

#[inline]
fn gauss2(x: f64, y: f64, s: f64) -> f64 {
	return (-(x*x + y*y)/(2.0*s*s)).exp();
}

#[inline]
fn b(src: &[f32], xsize: i32, ysize: i32, x: i32, y: i32, diameter: i32, kernel_d: &[f64], sigma_r: f64) -> f32 {
	let src_center: f32 = src[(xsize*y+x) as usize];
	let halfdiameter: i32 = diameter/2;
	let mut filtered: f64 = 0.0;
	let mut wsum: f64 = 0.0;

	if !src_center.is_finite() {
		return f32::NAN;
	}

	for j in -halfdiameter..halfdiameter {
		let y_offset: usize = (xsize * clamp(y+j, 0, ysize-1)) as usize;
		let j_offset: usize = ((j+halfdiameter) * diameter) as usize;
		for i in -halfdiameter..halfdiameter {
			let x_offset: usize = clamp(x+i, 0, xsize-1) as usize;
			let src_neighbour: f32 = src[y_offset+x_offset];
			if !src_neighbour.is_finite() {
				continue;
			}
			let w: f64 = kernel_d[j_offset+((i+halfdiameter) as usize)] * gauss((src_neighbour-src_center) as f64, sigma_r);
			filtered += (src_neighbour as f64) * w;
			wsum += w;
		}
	}

    return (filtered / wsum) as f32;
}

fn bilateral(dst: &mut [f32], src: &[f32], xsize: i32, ysize: i32, sigma_d: f64, sigma_r: f64) -> () {
	let mut diameter: i32 = (sigma_d*3.0) as i32;
	diameter += 1-diameter%2;
	let halfdiameter: i32 = diameter/2;

	let mut kernel_d = vec![0.0; (diameter*diameter) as usize];
	for y in -halfdiameter..halfdiameter {
		for x in -halfdiameter..halfdiameter {
			kernel_d[((y+halfdiameter)*diameter + x+halfdiameter) as usize] = gauss2(x as f64, y as f64, sigma_d);
		}
	}

	for y in 0..ysize {
		for x in 0..xsize {
			dst[(xsize*y+x) as usize] = b(src, xsize, ysize, x, y, diameter, &kernel_d, sigma_r);
		}
	}
}

fn main() {
	let input: [f32; 1024*1024] = include!("sigma.rs.in");
	let mut output: [f32; 1024*1024] = [0.0; 1024*1024];
	bilateral(&mut output, &input, 1024, 1024, 15.0, 70.0);
	//for y in 0..1024 {
	//	for x in 0..1024 {
	//		print!("{} ", output[y*1024 + x]);
	//	}
	//	print!("\n");
	//}
}
