#include <math.h>
#include <stdlib.h>
#include <stdio.h>
//#include <time.h>

#define MAX(x, y) ((x >= y) ? x : y)
#define MIN(x, y) ((x < y) ? x : y)
#define CLAMP(x, minval, maxval) (MIN((MAX(x, minval)), maxval))

#define GAUSS(x, s) (exp(-(x*x)/(2*s*s)))
#define GAUSS2(x, y, s) (exp(-(x*x + y*y) / (2*s*s)))

#include "sigma.h" // const char *input;

static inline float
b(	const float *restrict src,
	int xsize, int ysize,
	int x, int y,
	int diameter, double *restrict kernel_d, double sigma_r)
{
	const float *src_center = src + xsize*y + x;
	int halfdiameter = diameter/2;
	double filtered = 0;
	double wsum = 0;

	if (!isfinite(*src_center)) {
		return NAN;
	}

	for (int j = -halfdiameter; j <= halfdiameter; j++) {
		int y_offset = xsize * CLAMP(y+j, 0, ysize-1);
		int j_offset = j * diameter;
		for (int i = -halfdiameter; i <= halfdiameter; i++) {
			int x_offset = CLAMP(x+i, 0, xsize-1);
			const float *src_neighbour;
			double w;
			src_neighbour = src + y_offset + x_offset;
			if (!isfinite(*src_neighbour))
				continue;
			w = kernel_d[j_offset + i] * GAUSS((*src_neighbour - *src_center), sigma_r);
			filtered += (*src_neighbour) * w;
			wsum += w;
		}
	}

	return filtered / wsum;
}

void
bilateral(	float *restrict dst,
			const float *restrict src,
			int xsize, int ysize,
			double sigma_d, double sigma_r)
{
	int diameter, halfdiameter;
	double *kernel_d;

	diameter = (int)(sigma_d*3.0);
	diameter += 1-diameter%2;
	halfdiameter = diameter/2;

	kernel_d = alloca(diameter*diameter*sizeof(double));
	kernel_d += (diameter*diameter)/2;
	for (int y = -halfdiameter; y <= halfdiameter; y++) {
		for (int x = -halfdiameter; x <= halfdiameter; x++) {
			kernel_d[y*diameter + x] = GAUSS2(x, y, sigma_d);
		}
	}

	#pragma omp parallel for
	for (int y = 0; y < ysize; y++) {
		for (int x = 0; x < xsize; x++) {
			dst[xsize*y +x] = b(src, xsize, ysize, x, y, diameter, kernel_d, sigma_r);
		}
	}
}

int main(void)
{
	//struct timespec start, stop;
	float *output = NULL;

	output = malloc(1024*1024*sizeof(float));
	if (output == NULL)
		abort();
	//clock_gettime(CLOCK_MONOTONIC, &start);
	bilateral(output, input, 1024, 1024, 15.0, 70.0);
	//clock_gettime(CLOCK_MONOTONIC, &stop);
	free(output);
	//printf(">> %ld.%lds\n", (stop.tv_sec-start.tv_sec), (stop.tv_nsec-start.tv_nsec)/1000000);

	/*for (int y = 0; y < 1024; y++) {
		for (int x = 0; x < 1024; x++) {
			printf("%f", output[y*1024+x]);
		}
		printf("\n");
	}*/

	return 0;
}
